from django.core.paginator import Paginator
from django.shortcuts import render, get_object_or_404
from . import models, forms
from cart.forms import CartAddProductForm
from cart.cart import Cart  # need add for normal cart work
from django.contrib.postgres.search import SearchVector


def product_list(request, category_slug=None):
    cart = Cart(request)
    category = None
    products = models.Product.objects.filter(available=True)

    categories = models.Category.objects.all()
    brands = models.Brand.objects.all()

    if category_slug:
        category = get_object_or_404(models.Category, slug=category_slug)
        products = products.filter(category=category)

    # paginator work
    paginator = Paginator(products, 16)

    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)

    return render(request, 'index.html', {
        'category': category,
        'categories': categories,
        'products': products,
        'cart': cart,
        'brands': brands,
        'page_obj': page_obj,
    })


def category_list(request, category_slug):
    """Show's category page"""
    cart = Cart(request)  # need for cart in header works
    category = get_object_or_404(models.Category, slug=category_slug)
    # products = models.Product.objects.filter(category=category, available=True)
    brands = models.Brand.objects.all()

    # get all product by current parent category
    products = models.Product.objects.filter(category__in=category.get_descendants(include_self=True))

    request = request
    if 'price' in request.GET:
        min_price, max_price = request.GET['price'].split(',')
        products = products.filter(price__range=(min_price, max_price))

    if 'brands' in request.GET:
        selected_brands = [int(i) for i in request.GET.getlist('brands')]
        products = products.filter(brand__in=selected_brands)

    if 'categories' in request.GET:
        selected_categories = [int(i) for i in request.GET.getlist('categories')]
        filter_categories = []
        for category_id in selected_categories:
            category_obj = models.Category.objects.filter(id=category_id).first()
            filter_categories.extend(category_obj.get_descendants(include_self=True))
        products = products.filter(category__in=filter_categories)

    # need for menu works
    categories = models.Category.objects.all().order_by('position')

    # paginator work
    paginator = Paginator(products, 24)  # Show 25 contacts per page.

    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)

    return render(request, 'category.html', {
        'category': category,
        'categories': categories,
        'products': products,
        'cart': cart,
        'page_obj': page_obj,
        'brands': brands,
    })


def product_detail(request, pk, slug, category_slug):
    cart = Cart(request)  # need for cart in header works
    product = get_object_or_404(models.Product, id=pk, slug=slug, available=True, )
    cart_product_form = CartAddProductForm()
    categories = models.Category.objects.all().order_by('position')
    brands = models.Brand.objects.all()

    # Список активных комментариев для этой статьи.
    comments = product.comments.filter(active=True)
    new_comment = None

    if request.method == 'POST':
        # Пользователь отправил комментарий.
        comment_form = forms.CommentForm(data=request.POST)
        if comment_form.is_valid():
            # Создаем комментарий, но пока не сохраняем в базе данных.
            new_comment = comment_form.save(commit=False)
            # Привязываем комментарий к текущей product.
            new_comment.product = product
            # Сохраняем комментарий в базе данных.
            new_comment.save()
    else:
        comment_form = forms.CommentForm()

    return render(request, 'product_detail.html', {
        'product': product,
        'cart_product_form': cart_product_form,
        'categories': categories,
        'category_slug': category_slug,
        'comments': comments,
        'new_comment': new_comment,
        'comment_form': comment_form,
        'cart': cart,
        'brands': brands,
    })


def products_search(request):
    cart = Cart(request)  # need for cart in header works
    form = forms.SearchForm()
    query = None
    results = []
    brands = models.Brand.objects.all()

    if 'query' in request.GET:
        form = forms.SearchForm(request.GET)

    if form.is_valid():
        query = form.cleaned_data['query']
        results = models.Product.objects.annotate(search=SearchVector('name', 'description'), ).filter(search=query)
    return render(request, 'shop/search.html', {
        'form': form,
        'query': query,
        'results': results,
        'brands': brands,
        'cart': cart,
    })
