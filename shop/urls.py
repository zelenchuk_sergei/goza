from django.urls import path
from . import views

app_name = 'shop'

urlpatterns = [
    path('', views.product_list, name='product_list'),
    path('search/', views.products_search, name='products_search'),
    path('<slug:category_slug>', views.category_list, name='product_list_by_category'),
    path('<slug:category_slug>/<slug:slug>/<int:pk>/', views.product_detail, name='product_detail'),

]
