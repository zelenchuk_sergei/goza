from django.db import models
from mptt.models import MPTTModel
from mptt.fields import TreeForeignKey
from taggit.managers import TaggableManager
from django.urls import reverse


class Category(MPTTModel):
    parent = TreeForeignKey('self', on_delete=models.CASCADE, null=True, blank=True, related_name='children')
    name = models.CharField('Назва категорії', max_length=200, db_index=True)
    slug = models.SlugField("url", max_length=200, unique=True)
    position = models.IntegerField('Порядковий номер', blank=True, null=True)
    image = models.ImageField('Обкладинка для категорій', upload_to='categories/%Y/%m/%d', blank=True, null=True)
    small_image = models.ImageField('Обкладинка на головну', upload_to='categories/%Y/%m/%d', blank=True, null=True)
    available_on_main = models.BooleanField('Закріпити на головній?', default=False)

    class Meta:
        ordering = ('name',)
        verbose_name = 'категорія'
        verbose_name_plural = 'категорії'

    class MPTTMeta:
        order_insertion_by = ['name']

    def __str__(self):
        full_path = [self.name]
        k = self.parent
        while k is not None:
            full_path.append(k.name)
            k = k.parent
        return '  /  '.join(full_path[::-1])

    def get_absolute_url(self):
        return reverse('shop:product_list_by_category', args=[str(self.slug)])


class Brand(models.Model):
    name = models.CharField('Ім\'я', max_length=255)
    image = models.ImageField('Фото', upload_to='brands/', blank=False)

    class Meta:
        verbose_name = 'Бренд'
        verbose_name_plural = 'Бренды'
        ordering = ('name',)

    def __str__(self):
        return self.name


class Product(models.Model):
    category = models.ForeignKey(Category, verbose_name='Назва категорії', related_name='products',
                                 on_delete=models.CASCADE)

    vendor_code = models.CharField('Артикул', max_length=200, db_index=True)
    name = models.CharField('Назва товару', max_length=200, db_index=True)
    slug = models.SlugField('url', max_length=200, db_index=True)
    full_image = models.ImageField('Велике фото', upload_to='products/%Y/%m/%d', blank=True)
    small_image = models.ImageField('Мале фото', upload_to='products/%Y/%m/%d', blank=True)
    small_description = models.TextField("Короткий опис", blank=True)
    description = models.TextField('Опис товару', blank=True)
    price = models.DecimalField('Кінцева ціна', max_digits=10, decimal_places=2)
    old_price = models.DecimalField('Cтара ціна (перекреслена)', max_digits=10, decimal_places=2, blank=True)
    available = models.BooleanField('Доступність', default=True)
    tags = TaggableManager(blank=True)
    quantity = models.PositiveIntegerField(blank=True, null=True)

    seo_description = models.CharField("SEO-опис", max_length=200, blank=True)

    created = models.DateTimeField('Дата створення', auto_now_add=True)
    updated = models.DateTimeField('Остання правка', auto_now=True)

    brand = models.ForeignKey(Brand, on_delete=models.SET_NULL, related_name='brand', blank=True, null=True)

    class Meta:
        ordering = ('name',)
        index_together = (('id', 'slug',),)
        verbose_name = 'товар'
        verbose_name_plural = 'товари'

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('shop:product_detail', args=[str(self.category.slug), str(self.slug), self.pk])


class Comment(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE, related_name='comments')
    name = models.CharField('Ім\'я', max_length=80)
    email = models.EmailField('Email', )
    body = models.TextField('Відгук', )
    active = models.BooleanField('Публічний', default=True)

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = 'Коментар'
        verbose_name_plural = 'Коментарі'
        ordering = ('created',)

    def __str__(self):
        return 'Коментар від {} про товар {} із ID = {}'.format(self.name, self.product.name, self.product.pk)
