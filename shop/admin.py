from tinymce.widgets import TinyMCE
from django.db import models
from django.contrib import admin
from .models import Category, Product, Comment, Brand
from mptt.admin import DraggableMPTTAdmin


@admin.register(Category)
class Category2Admin(DraggableMPTTAdmin):
    mptt_indent_field = "name"
    list_display = ('tree_actions', 'indented_title',
                    'related_products_count', 'related_products_cumulative_count')
    list_display_links = ('indented_title',)

    prepopulated_fields = {'slug': ('name',), }

    def get_queryset(self, request):
        qs = super().get_queryset(request)

        # Add cumulative product count
        qs = Category.objects.add_related_count(
                qs,
                Product,
                'category',
                'products_cumulative_count',
                cumulative=True)

        # Add non cumulative product count
        qs = Category.objects.add_related_count(qs,
                 Product,
                 'category',
                 'products_count',
                 cumulative=False)
        return qs

    def related_products_count(self, instance):
        return instance.products_count
    related_products_count.short_description = 'Related products (for this specific category)'

    def related_products_cumulative_count(self, instance):
        return instance.products_cumulative_count
    related_products_cumulative_count.short_description = 'Related products (in tree)'


@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    list_display = ['name', 'category', 'price', 'available', 'created', ]
    list_filter = ['available', 'created', ]
    list_editable = ['price', 'available', ]
    prepopulated_fields = {'slug': ('name', ), }

    formfield_overrides = {
        models.TextField: {'widget': TinyMCE()},
    }


@admin.register(Comment)
class CommentAdmin(admin.ModelAdmin):
    list_display = ['product', 'body', 'email', 'active', ]
    list_filter = ['active', 'created', ]
    list_editable = ['active', ]


@admin.register(Brand)
class BrandAdmin(admin.ModelAdmin):
    list_display = ['name', 'image' ]
    list_filter = ['name' ]
    list_editable = []
