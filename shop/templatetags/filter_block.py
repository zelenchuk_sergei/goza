#-*- coding: utf-8 -*-
from django import template
from django.db.models import Max, Min

from shop.models import Product, Brand, Category

register = template.Library()

@register.filter(name='get_item')
def get_item(dictionary, key):
    return dictionary.get(key)

@register.inclusion_tag('blocks/filter.html', takes_context=True)
def filter(context, params={}, *args, **kwargs):
    """
    :param context: Контекст вьюса
    :param params: Параметры фильтра
    :param args:
    :param kwargs:
    :return:
    """

    if 'category' in context and context['category']:
        products = Product.objects.filter(category__in=context['category'].get_descendants(include_self=True))
        categories = Category.objects.filter(parent=context['category'])
    else:
        products = Product.objects.all()
        categories = Category.objects.filter(parent__isnull=True)

    min_price = products.aggregate(Min('price'))['price__min']
    max_price = products.aggregate(Max('price'))['price__max']

    brands_list = products.values_list('brand', flat=True)
    brand_quantities = {}
    for brand in brands_list:
        if brand in brand_quantities:
            brand_quantities[brand] += 1
        else:
            brand_quantities[brand] = 1
    if brands_list:
        brands = Brand.objects.filter(id__in=brands_list)
    else:
        brands = []

    request = context['request'].GET
    if 'price' in request:
        select_min_price, select_max_price = request['price'].split(',')
    else:
        select_min_price = min_price
        select_max_price = max_price

    selected_brands = {}
    if 'brands' in request:
        selected_brands = {int(i): True for i in request.getlist('brands')}

    selected_categories = {}
    if 'categories' in request:
        selected_categories = {int(i): True for i in request.getlist('categories')}

    return {
        'min_price': min_price,
        'max_price': max_price,
        'select_min_price': select_min_price,
        'select_max_price': select_max_price,
        'brands': brands,
        'brand_quantities': brand_quantities,
        'selected_brands': selected_brands,
        'categories': categories,
        'selected_categories': selected_categories,
    }
