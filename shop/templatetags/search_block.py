#-*- coding: utf-8 -*-
from django import template

from shop.models import Category

register = template.Library()

@register.inclusion_tag('blocks/search.html', takes_context=True)
def search(context, params={}, *args, **kwargs):
    """
    :param context: Контекст вьюса
    :param params: Параметры фильтра
    :param args:
    :param kwargs:
    :return:
    """

    categories = Category.objects.filter(parent__isnull=True)
    
    return {
        'categories': categories
    }
