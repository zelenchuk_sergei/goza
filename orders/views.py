from django.shortcuts import render
from .models import OrderItem
from .forms import OrderCreateForm
from cart.cart import Cart
from .tasks import order_created

from django.core.mail import send_mail
from django.core.mail import EmailMultiAlternatives
from .models import Order

from shop.models import Category


def order_create(request):
    cart = Cart(request)
    categories = Category.objects.all().order_by('position')
    if request.method == 'POST':
        form = OrderCreateForm(request.POST)
        if form.is_valid():
            order = form.save()
            if request.user:
                order.user = request.user
                order.save()
            for item in cart:
                OrderItem.objects.create(order=order,
                                         product=item['product'],
                                         price=item['price'],
                                         quantity=item['quantity'])
            # очистка корзины
            cart.clear()
            # запуск асинхронной задачи
            # order_created.delay(order.id)
            order_created(order.id)

            # send email alert to sales manager
            order_email_alert(order.id, order.first_name, order.mobile, 'zelenchyks@gmail.com')
            order_email_alert(order.id, order.first_name, order.mobile, 'gozacomua@gmail.com')

            return render(request, 'orders/created.html', {
                'order': order,
                'categories': categories,
            })
    else:
        form = OrderCreateForm
    return render(request, 'orders/create.html', {
        'cart': cart,
        'form': form,
        'categories': categories,

    })


def order_created(order_id):
    """Задача отправки email-уведомлений при успешном оформлении заказа."""
    order = Order.objects.get(id=order_id)
    subject = 'GoZa | Замовлення №. {}'.format(order.id)
    message = """
        Вітаємо {},\n\nВи успішно залишили замовлення. Номер замовлення - {}.\n
        
        Наш Досвід - Ваш Результат.
        Вирощуй Легко, Замовляй Безпечно.
        Go Za Покупками!\n

        Україна, Херсон, Вул. Маяковського, 26, 73009
        goza.com.ua
        
        +380 (50) 990 76 78 
        +380 (96) 292 50 28  
        
        gozacomua@gmail.com
        
        """.format(order.first_name, order.id)
    mail_sent = send_mail(subject, message, 'zelenchyks@gmail.com', [order.email])
    return mail_sent


def order_email_alert(order_id, name, mobile, who_take):
    try:
        subject, from_email, to = 'Новая заявка', 'zelenchyks@gmail.com', who_take
        text_content = 'Новая заявка на сайте GoZa'
        html_content = '''
                    <h1>Новая заявка (№%s) на сайте GoZa</h1>
                    <table>
                         <tr>
                            <td><b>Имя клиента:</b></td>
                            <td>&nbsp;&nbsp;%s</td>
                        </tr>

                         <tr>
                            <td><b>Мобильный:</b></td>
                            <td>&nbsp;&nbsp;%s</td>
                        </tr>
                    </table>
                    
                     <a href='https://goza.com.ua/admin/orders/order/%s'>
                        <b>===>>> Открыть заявку на сайте <<<===</b>
                    </a>
                    
                    
                    
                    <hr>
                    
                    <br>
                    
                    Для вас старается Зеленчук Сергей<br>
                    <a href="https://t.me/zelenchuk_sergei">Обратная связь</a>
                    
                    
                    ''' % (order_id, name, mobile, order_id)

        msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
        msg.attach_alternative(html_content, "text/html")
        msg.send()
    except Warning:
        print('Huston we have a problems with smtp')
