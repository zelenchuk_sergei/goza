# from django.contrib.auth.models import User
from django.db import models
from shop.models import Product


class Order(models.Model):
    first_name = models.CharField('Ім\'я', max_length=50)
    last_name = models.CharField('Прізвище', max_length=50)
    email = models.EmailField('Email', blank=True, null=True)
    mobile = models.CharField('Мобільний', max_length=250)
    city = models.CharField('Населений пункт', max_length=100)
    # user = models.ForeignKey(User, related_name='Пользователь', on_delete=models.SET_NULL, blank=True, null=True)
    delivery = models.CharField('Спосіб доставки', max_length=250, blank=True, null=True)
    delivery_detail = models.CharField('Номер відділлення', max_length=250, blank=True, null=True)

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    paid = models.BooleanField(default=False)

    class Meta:
        ordering = ('-created',)
        verbose_name = 'Заказ'
        verbose_name_plural = 'Заказы'

    def __str__(self):
        return 'Order {}'.format(self.id)

    def get_total_cost(self):
        return sum(item.get_cost() for item in self.items.all())


class OrderItem(models.Model):
    order = models.ForeignKey(Order, related_name='items', on_delete=models.CASCADE)
    product = models.ForeignKey(Product, related_name='order_items', on_delete=models.CASCADE)
    price = models.DecimalField(max_digits=10, decimal_places=2)
    quantity = models.PositiveIntegerField(default=1)

    def __str__(self):
        return '{}'.format(self.id)

    def get_cost(self):
        return self.price * self.quantity
