# Описание
Предназначен для запуска, тестирования и доработок на локальной машине

# Запуск
1. из корня проекта - docker-compose up -d`
2. накатить базу в postgres. __TODO__: добавить в проект тестовую
3. поменять в настройках коннект к БД с `localhost` на `postgres` __TODO__: реализовать подмену хоста в докере
4. в бразуере открыть `localhost`

# Тесты pylint
1. зайти в докер `docker exec -it goza_site_1 bash`
`goza_site_1` - название контейнера, может отличаться. список контейнеров - `docker ps`
2. запустить `pylint --load-plugins pylint_django ./goza` где `goza` - название модуля (менять на `account`, `cart` и т.д.)
__TODO__: автоматизировать проверку через одну команду
__TODO__: отключить некоторые проверки правил
