from django import forms


class CartAddProductForm(forms.Form):
    quantity = forms.IntegerField(min_value=1)
    update = forms.BooleanField(required=False, initial=False, widget=forms.HiddenInput)

    def __init__(self, post = None, max_value = None, *args, **kwargs):
        super(CartAddProductForm, self).__init__(post, *args, **kwargs)
        if max_value:
            self.fields['quantity'] = forms.IntegerField(max_value=max_value)
